# MongoDB Rails Test

This project is a test project to review and practice the use of MongoDB combined with rails.

## General info

* Version 0.0?
* Website: [appco.mx](www.appco.mx)
* Lead architect: [saul.ortigoza@appco.mx](mailto:saul.ortigoza@appco.mx)
* Repo owner: [saul.ortigoza@appco.mx](mailto:saul.ortigoza@appco.mx)
* Team contact: [contacto@appco.mx](mailto:contacto@appco.mx)
* Team repo: [bitbucket appco](https://bitbucket.org/appco/)
* Team project admin: [trello appco](https://trello.com/appco2)

## General dependencies

* Git
* Ruby > 2.0
* SQLite for development
* MySQL for production
* MongoDB

## Architecture and design notes

### Initialization script used

	rails generate devise:install
	rails generate devise User
	rails g cancan:ability
	rake db:migrate
	rails g scaffold_controller Story

## References

* http://docs.mongodb.org/ecosystem/tutorial/ruby-mongoid-tutorial/
* https://thinkster.io/angular-rails#introduction
* http://scrollmagic.io/
* http://scrollmagic.io/examples/advanced/parallax_scrolling.html
