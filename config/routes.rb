Rails.application.routes.draw do
  
  get 'angular/index'

  devise_for :users

  root to: 'welcome#index'

  resources :stories

  get '/angular', to: 'angular#index'
  
end
