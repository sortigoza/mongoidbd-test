class Address
  include Mongoid::Document
  include Mongoid::Timestamps

  field :street_name, type: String
  field :number, type: String
  field :address_line1, type: String
  field :address_line2, type: String
  field :city, type: String
  field :state_province, type: String
  field :postal_code, type: String
  field :country, type: String
  field :latitude, type: Float
  field :longitude, type: Float

  # Cached values.

  # Note this: ids are of class ObjectId.

  # Relationships.
  belongs_to :user
  belongs_to :story

  # Validations.
end
