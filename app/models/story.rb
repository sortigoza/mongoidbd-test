class Story
 include Mongoid::Document
 include Mongoid::Timestamps

 field :title,     type: String
 field :url,       type: String
 field :slug,      type: String
 field :voters,    type: Array
 field :votes,     type: Integer, :default => 0
 field :relevance, type: Integer, :default => 0

 # Cached values.
 field :comment_count, type: Integer, :default => 0
 field :username,      type: String

 # Note this: ids are of class ObjectId.

 # Relationships.
 belongs_to :user
 has_one :address

 # Validations.
 validates_presence_of :title
end