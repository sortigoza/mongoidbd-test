module ApplicationHelper
  def site_name
    "MongoidDB Test"
  end

  def site_url
    if Rails.env.production?
      # Place your production URL in the quotes below
      "http://TBD.com"
    else
      # Our dev & test URL
      "http://localhost:3000/"
    end
  end

  def meta_author
    # Change the value below between the quotes.
    "AppCo"
  end

  def meta_description
    # Change the value below between the quotes.
    "description TBD"
  end

  def meta_keywords
    # Change the value below between the quotes.
    "AppCo WebApp"
  end

  def full_title(page_title)
    if page_title.empty?
      site_name
    else
      "#{page_title} | #{site_name}"
    end
  end

  def bootstrap_class_for flash_type
    { success: "alert-success", error: "alert-danger", alert: "alert-warning", notice: "alert-info" }[flash_type.to_sym] || flash_type.to_s
  end

end
