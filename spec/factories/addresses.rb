# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :address do
    street_name "MyString"
    number "MyString"
    address_line1 "MyString"
    address_line2 "MyString"
    city "MyString"
    state_province "MyString"
    postal_code "MyString"
    country "MyString"
    latitude "MyString"
    longitude "MyString"
  end
end
